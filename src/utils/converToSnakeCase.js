export const converToSnakeCase = (str) => {
  str = str.replace(/[_]/g, ' ')
  // all the capital letters except the first
  const snake = /\B[A-Z]\B/g
  // add ' ' to matches without removing the character
  str = str.replace(snake, ' $&')
  return str
}
