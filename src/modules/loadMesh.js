import { GLTFLoader } from 'three/examples/jsm/loaders/GLTFLoader.js'
import { DRACOLoader } from 'three/examples/jsm/loaders/DRACOLoader.js'

import { scene, skyMat } from '../script.js'

// Configure and create Draco decoder.
const dracoLoader = new DRACOLoader()
dracoLoader.setDecoderPath('assets/js/libs/draco/')
// dracoLoader.setDecoderConfig()
const loader = new GLTFLoader()
loader.setDRACOLoader(dracoLoader)

const loadMesh = (filepath) => {
  console.log(filepath)
  return new Promise((resolve, reject) => {
    loader.load(filepath, function (gltf) {
      gltf.scene.scale.set(6, 6, 6)
      scene.add(gltf.scene)
      resolve(gltf)
      // dracoLoader.dispose()
      gltf.scene.traverse((child) => {
        if (child.isMesh) {
          console.log('mesh found')
          // child.material = skyMat
        }
      })
    })
  })
}
export { loadMesh }
