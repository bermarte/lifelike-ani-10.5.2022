import * as THREE from 'three'

import gsap from 'gsap'
import { scene } from '../script.js'

import { GLTFLoader } from 'three/examples/jsm/loaders/GLTFLoader'
import { DRACOLoader } from 'three/examples/jsm/loaders/DRACOLoader'
import { MeshSurfaceSampler } from 'three/examples/jsm/math/MeshSurfaceSampler'

// Shaders
import vertexShader from './vertex.glsl'
import fragmentShader from './fragment.glsl'

class Model {
  constructor (object) {
    const { name, file, scene, placeOnLoad, colors, background } = object
    this.name = name
    this.file = file
    this.scene = scene
    this.placeOnLoad = placeOnLoad
    this.background = background

    this.isActive = false

    this.color1 = colors[0]
    this.color2 = colors[1]

    this.loader = new GLTFLoader() // Instantiate a loader
    this.dracoLoader = new DRACOLoader() // DRACOLoader instance to decode compressed mesh data
    
    this.dracoLoader.setDecoderPath('./draco/')
    this.loader.setDRACOLoader(this.dracoLoader)
    this.init()
  }

  init () {
    this.loader.load(this.file, (response) => {
      /**
       * Mesh
       */
      this.mesh = response.scene.children[0]
      this.model = response.scene
      console.log('model', this.mesh)
    //   if (this.model.children[0].name === 'Cube') {
    //     this.mixer = new THREE.AnimationMixer(this.model)
    //     console.log('animations', response.animations)
    //     this.clip = response.animations[0]
    //     this.action = this.mixer.clipAction(this.clip)
    //     this.action.play()
    //   }

      /**
       * Material
       */
      this.material = new THREE.MeshBasicMaterial({
        color: 'red',
        wireframe: true
      })
      this.mesh.material = this.material

      /**
       * Geometry
       */
      this.geometry = this.mesh.geometry

      /**
       * Particles material
       */
      //   this.particlesMaterial = new THREE.PointsMaterial({
      //     color: 'red',
      //     size: 0.02,
      //   })

     
      this.particlesMaterial = new THREE.ShaderMaterial({
        vertexShader,
        fragmentShader,
        transparent: true,
        depthTest: true,
        depthWrite: true,
        blending: THREE.AdditiveBlending,
        uniforms: {
          uColor1: { value: new THREE.Color(this.color1) },
          uColor2: { value: new THREE.Color(this.color2) },
          uTime: { value: 1 },
          uScale: { value: 1 }
        }
      })

      /**
       * Particles geometry
       */
      // Create a sampler for a Mesh surface.
      const sampler = new MeshSurfaceSampler(this.mesh)
        .setWeightAttribute('color')
        .build()

      const numberParticles = 20000

      this.particlesGeometry = new THREE.BufferGeometry()

      const particlesPosition = new Float32Array(numberParticles * 3) // x,y,z
      const particlesRandomness = new Float32Array(numberParticles * 3)

      for (let i = 0; i < numberParticles; i++) {
        const newPosition = new THREE.Vector3()
        sampler.sample(newPosition)

        particlesPosition.set(
          [newPosition.x, newPosition.y, newPosition.z],
          i * 3
        )

        particlesRandomness.set(
          [
            Math.random() * 2 - 1, // -1 to 1
            Math.random() * 2 - 1,
            Math.random() * 2 - 1
          ],
          i * 3
        )
      }

      this.particlesGeometry.setAttribute(
        'position',
        new THREE.BufferAttribute(particlesPosition, 3)
      )
      this.particlesGeometry.setAttribute(
        'aRandom',
        new THREE.BufferAttribute(particlesRandomness, 3)
      )
      console.log('hi from shaderModel')
      console.log(this.particlesGeometry)

      /**
       * Particles
       */
      this.particles = new THREE.Points(
        this.particlesGeometry,
        this.particlesMaterial
      )
      // this.add()
    if (this.placeOnLoad) this.add()
    })
  }

  add () {
    this.isActive = true
    this.scene.add(this.mesh)
    // this.scene.add(this.particles)
    console.log('ADDED')
    // this.particlesMaterial.uniforms.uScale *= 1.0
    // this.particlesMaterial.uniforms.uScale = 1.0
    // console.log('USCALE', this.particlesMaterial)
    // this.particlesMaterial.uniforms.uScale.value = 1
    // console.log('val', this.particlesMaterial.uniforms.uScale.value)
   gsap.to(this.particlesMaterial.uniforms.uScale, {
      value: 1,
      duration: 0.8,
      ease: 'power3.out'
    })

    if (!this.isActive) {
      alert('not')
      gsap.fromTo(
        this.particles.rotation,
        {
          y: Math.PI
        },
        {
          y: 0,
          duration: 0.8,
          ease: 'power3.out'
        }
      )

      gsap.to('body', {
        background: this.background,
        duration: 0.8
      })
    }

    this.isActive = true
    console.log('last')
  }
 
  //remove () {
  //   gsap.to(this.particlesMaterial.uniforms.uScale, {
  //     value: 0,
  //     duration: 0.8,
  //     ease: 'power3.out',
  //     onComplete: () => {
  //       this.scene.remove(this.particles)
  //       this.isActive = false
  //     }
  //   })

  //   gsap.to(this.particles.rotation.uScale, {
  //     y: Math.PI,
  //     duration: 0.8,
  //     ease: 'power3.out'
  //   })
  //}

}

export default Model
