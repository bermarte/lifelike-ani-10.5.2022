import './css/main.css'
import * as THREE from 'three'

import { plane } from './modules/plane.js'
import { sizes } from './utils/sizes.js'
import { handleResize } from './utils/handleResize.js'
import { camera } from './modules/camera.js'
import { blenderCamera, loadBlenderCamera } from './modules/blenderCamera.js'
import { loadMesh } from './modules/loadMesh.js'
import { getJSON } from './utils/getJson.js'
import { ScrollTrigger } from 'gsap/dist/ScrollTrigger'
// import { EXRLoader } from 'three/examples/jsm/loaders/EXRLoader'
// import VirtualScroll from 'virtual-scroll'
import gsap from 'gsap'
import { EffectComposer } from 'three/examples/jsm/postprocessing/EffectComposer.js'
import { RenderPass } from 'three/examples/jsm/postprocessing/RenderPass.js'
import { UnrealBloomPass } from 'three/examples/jsm/postprocessing/UnrealBloomPass.js'
// eslint-disable-next-line no-unused-vars
import { GlitchPass } from 'three/examples/jsm/postprocessing/GlitchPass.js'
gsap.registerPlugin(ScrollTrigger)

const COLOR_1 = 0xD7FF00
const COLOR_2 = 0xFFFFFF
const COLOR_3 = 0x00ffbf
const COLOR_4 = 0xbfafdd

// https://stackoverflow.com/questions/50226091/how-three-js-uses-the-gradient-color-for-the-background

// check also https://stackoverflow.com/questions/64560154/applying-color-gradient-to-material-by-extending-three-js-material-class-with-on
// https://stackoverflow.com/questions/20307489/three-js-glsl-convert-pixel-coordinate-to-world-coordinate?noredirect=1&lq=1
const vertexShader = `
      varying vec3 vWorldPosition;
      void main() {
        
        vec4 worldPosition = modelMatrix * vec4( position, 1.0 );
        vWorldPosition = worldPosition.xyz;
        gl_Position = projectionMatrix * modelViewMatrix * vec4( position, 1.0 );
       }`

const fragmentShader = `
     
      varying vec3 vWorldPosition;
      uniform float u_time;
      uniform vec3 color_1;
      uniform vec3 color_2;
      uniform vec3 color_3;
      uniform vec3 color_4;
      void main() {
        vec3 color = vec3(0.0);
        float h = normalize( vWorldPosition).z;
        // vec3 colorA = vec3(0.149,0.141,0.912);
        vec3 colorA = vec3(color_1);
        vec3 colorB = vec3(color_2);
        vec3 colorC = vec3(color_3);
        vec3 colorD = vec3(color_4);
        float pct = abs(sin(u_time));
        // color = mix(mix(colorA, colorB, max( h/3.0, 0.25 )), mix(colorC, colorD, max( h/(3.0*2.0),0.5 )),  max( h, 0.8 ));
        // gl_FragColor = vec4(color, 1.0);
        // gl_FragColor = vec4( mix( colorA, colorB, h/ 0.1 ), 1.0 );
        vec3 col_1 =  mix( colorA, colorB, h/ 0.1 );
        vec3 col_2 =   mix( colorC, colorD, h/ 0.4 );
        gl_FragColor = vec4( mix( col_1, col_2, h ), 1.0 );
    
      }`
const uniforms = {
  color_1: { value: new THREE.Color(COLOR_1) },
  color_2: { value: new THREE.Color(COLOR_2) },
  color_3: { value: new THREE.Color(COLOR_3) },
  color_4: { value: new THREE.Color(COLOR_4) }
}

const skyMat = new THREE.ShaderMaterial({
  uniforms,
  vertexShader,
  fragmentShader
})

const phongMaterial = new THREE.MeshPhongMaterial({
  color: 0xFF0000,
  flatShading: true
})

// show/hide console.log
const log = false
// const showRun = true

// Canvas
const canvas = document.querySelector('.c-canvas')

// Scene
const scene = new THREE.Scene()
// scene.background = new THREE.Color(0x82E4053) // #85C1E9

const path = 'assets/models/'
getJSON('data/objects.json').then(
  (data) => {
    data.objects.forEach(item => {
      console.log('loaded', item.name)
      return loadMesh(`${path}${item.file}`)
    })
  }
)

// Load camera
let mixTl
let composer

loadBlenderCamera()
  .then((object) => {
    scene.add(object.scene)
    const model = object.scene
    const mixer = new THREE.AnimationMixer(model)
    const clip1 = object.animations[0]
    const action1 = mixer.clipAction(clip1)
    action1.play()
    mixTl = mixer

    // post effects
    composer = new EffectComposer(renderer)
    composer.addPass(new RenderPass(scene, blenderCamera.cameras[0]))
    // composer.addPass(new GlitchPass())
    // map map  intensity radius
    composer.addPass(new UnrealBloomPass({ x: 1024, y: 1024 }, 1.5, 0.8, 0.85))
  })

const percentage = {
  run: 0
}

const tl = gsap.timeline()
  .to(percentage, {
    // last frame
    run: 17.5
  })

ScrollTrigger.create({
  trigger: '.c-container',
  start: '0% 0%',
  scrub: 2,
  animation: tl
})

// Animate camera along curve
const moveCamera = () => {
  return percentage.run
}

// Camera
camera.lookAt(plane.position)
scene.add(camera)
//

// ourMesh.add()

window.addEventListener('resize', handleResize)
// eslint-disable-next-line no-unused-vars
const clock = new THREE.Clock()

/**
 * Renderer
 */

const renderer = new THREE.WebGLRenderer({
  canvas: canvas,
  antialias: true,
  alpha: true
})

renderer.outputEncoding = THREE.sRGBEncoding
renderer.setSize(sizes.width, sizes.height)
renderer.setPixelRatio(Math.min(window.devicePixelRatio, 2))

const color = 0xFFFFFF
const intensity = 3
const light = new THREE.AmbientLight(color, intensity)
scene.add(light)
const tick = () => {
  if (blenderCamera) {
    const activeCam = blenderCamera.cameras[0]
    // instead of renderer.render(scene, activeCam) use effect composer
    renderer.render(scene, activeCam)
    // use effect composer
    composer.render()
    activeCam.aspect = sizes.width / sizes.height

    activeCam.updateProjectionMatrix()

    if (mixTl) {
      mixTl.setTime(moveCamera())
    }
  }
  // composer.render()
  // Call tick again on the next frame
  window.requestAnimationFrame(tick)
}

tick()

export { phongMaterial, skyMat, log, percentage, camera, renderer, scene }
